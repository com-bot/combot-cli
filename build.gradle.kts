import java.util.Properties

val projectSettings = fetchProjectSettings()

group = "org.combot"
version = if (projectSettings.isDevVer) "${projectSettings.libVer}-dev" else projectSettings.libVer

plugins {
    kotlin("multiplatform") version "1.4.31"
    `maven-publish`
}

repositories {
    mavenLocal()
    mavenCentral()
}

kotlin {
    explicitApi()
    val combotVer = projectSettings.libVer
    val kotlinVer = "1.4.31"
    val coroutinesVer = "1.4.3"

    linuxX64 {
        compilations.getByName("main") {
            dependencies {
                implementation("$group:combot-core:$combotVer")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVer")
            }
        }
        compilations.getByName("test") {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVer")
                implementation(kotlin("test", kotlinVer))
            }
        }
    }

    linuxArm32Hfp("linuxArm32") {
        compilations.getByName("main") {
            dependencies {
                implementation("$group:combot-core:$combotVer")
                // TODO: Add dependency on KotlinX Coroutines Core when it is available.
            }
        }
    }

    sourceSets {
        commonMain {
            dependencies {
                implementation(kotlin("stdlib-common", kotlinVer))
                implementation("$group:combot-core:$combotVer")
            }
        }
    }
}

data class ProjectSettings(val libVer: String, val isDevVer: Boolean)

fun fetchProjectSettings(): ProjectSettings {
    var libVer = "SNAPSHOT"
    var isDevVer = true
    val properties = Properties()
    file("project.properties").inputStream().use { inputStream ->
        properties.load(inputStream)
        libVer = properties.getProperty("libVer") ?: "SNAPSHOT"
        isDevVer = (properties.getProperty("isDevVer") ?: "false").toBoolean()
    }
    return ProjectSettings(libVer = libVer, isDevVer = isDevVer)
}
