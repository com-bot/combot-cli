@file:Suppress("EXPERIMENTAL_API_USAGE")

package org.combot.cli

internal expect fun fetchUserId(user: String): UInt
internal expect fun fetchGroupId(group: String): UInt
internal expect fun fetchUserName(uid: UInt): String
internal expect fun fetchGroupName(gid: UInt): String
