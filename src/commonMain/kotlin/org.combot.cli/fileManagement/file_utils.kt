@file:Suppress("EXPERIMENTAL_API_USAGE")

package org.combot.cli.fileManagement

import org.combot.core.fileManagement.authorization.FilePermissions

internal expect fun createFilePath(parentDirPath: String, fileName: String): String

internal expect fun changeFileOwnership(filePath: String, gid: UInt, uid: UInt): Boolean

internal expect fun fetchFileGroup(filePath: String): String

internal expect fun fetchFileGroupId(filePath: String): Int

internal expect fun fetchFileUser(filePath: String): String

internal expect fun fetchFileUserId(filePath: String): Int

internal expect fun changeFilePermissions(filePath: String, permissions: FilePermissions): Boolean
