package org.combot.cli.fileManagement

import org.combot.core.fileManagement.Directory

public expect class LocalDirectory(parentDirPath: String, name: String) : Directory
