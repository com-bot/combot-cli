package org.combot.cli.fileManagement

import org.combot.core.fileManagement.PlainFile

@Suppress("EXPERIMENTAL_API_USAGE")
public expect class LocalPlainFile(name: String, parentDirPath: String, isBinaryFile: Boolean = false) : PlainFile
