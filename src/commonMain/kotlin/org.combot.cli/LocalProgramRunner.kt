package org.combot.cli

import org.combot.core.ProgramRunner

public expect class LocalProgramRunner(
    programName: String,
    programArgs: Array<String> = emptyArray(),
    programDir: String = "",
    workingDir: String = ""
) : ProgramRunner
