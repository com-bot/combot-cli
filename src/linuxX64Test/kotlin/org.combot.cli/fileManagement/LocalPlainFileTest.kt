package org.combot.cli.fileManagement

import kotlinx.cinterop.toKString
import kotlinx.coroutines.runBlocking
import org.combot.core.fileManagement.authorization.FilePermissionType
import org.combot.core.fileManagement.authorization.FilePermissions
import platform.posix.getenv
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

@ExperimentalUnsignedTypes
class LocalPlainFileTest {
    @Test
    fun `read all text succeeds`() {
        val homeDir = getenv("HOME")?.toKString() ?: ""
        val file = LocalPlainFile(parentDirPath = homeDir, name = ".profile")
        runBlocking {
            val (success, data) = file.readAllText()
            println("File Contents (trimmed for brevity):")
            data.take(8).forEach(::println)
            assertTrue(success)
            assertTrue(data.isNotEmpty(), "No data read from text file")
        }
    }

    @Test
    fun `read all text fails`() {
        val file = LocalPlainFile(parentDirPath = "", name = "non_existing_file")
        runBlocking {
            val (success, data) = file.readAllText()
            assertFalse(success)
            assertTrue(data.isEmpty(), "The data variable should be empty")
        }
    }

    @Test
    fun `file creation succeeds`() {
        val file = LocalPlainFile(parentDirPath = "/tmp", name = "combot_test")
        runBlocking {
            assertTrue(file.create())
            file.delete()
        }
    }

    @Test
    fun `file creation fails`() {
        val file = LocalPlainFile(parentDirPath = "/var", name = "combot_test")
        runBlocking {
            assertFalse(file.create())
        }
    }

    @Test
    fun `file is deleted`() {
        val file = LocalPlainFile(parentDirPath = "/tmp", name = "combot_test")
        runBlocking {
            file.create()
            assertTrue(file.delete())
        }
    }

    @Test
    fun `file permissions changed`() {
        val file = LocalPlainFile(parentDirPath = "/tmp", name = "combot_test")
        val permissions = FilePermissions(
            owner = FilePermissionType.READ_WRITE,
            group = FilePermissionType.READ_WRITE,
            other = FilePermissionType.NONE
        )
        runBlocking {
            file.create()
            assertTrue(file.changePermissions(permissions))
            file.delete()
        }
    }

    @Test
    fun `invalid file`() {
        val file = LocalPlainFile(parentDirPath = "/var", name = "combot_test")
        runBlocking {
            assertFalse(file.isValid())
        }
    }

    @Test
    fun `valid file`() {
        val homeDir = getenv("HOME")?.toKString() ?: ""
        val file = LocalPlainFile(parentDirPath = homeDir, name = ".profile")
        runBlocking {
            assertTrue(file.isValid())
        }
    }
}
