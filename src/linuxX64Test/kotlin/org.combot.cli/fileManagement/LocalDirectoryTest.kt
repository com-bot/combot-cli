package org.combot.cli.fileManagement

import kotlinx.cinterop.toKString
import kotlinx.coroutines.runBlocking
import org.combot.core.fileManagement.authorization.FilePermissionType
import org.combot.core.fileManagement.authorization.FilePermissions
import platform.posix.getenv
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

@ExperimentalUnsignedTypes
class LocalDirectoryTest {
    @Test
    fun `directory is created`() {
        val dir = LocalDirectory(parentDirPath = "/tmp", name = "combot_temp")
        runBlocking {
            assertTrue(dir.create())
            dir.delete()
        }
    }

    @Test
    fun `directory creation fails`() {
        val dir = LocalDirectory(parentDirPath = "/var", name = "combot_temp")
        runBlocking {
            assertFalse(dir.create())
            assertFalse(dir.delete())
        }
    }

    @Test
    fun `directory is deleted`() {
        val dir = LocalDirectory(parentDirPath = "/tmp", name = "combot_temp")
        runBlocking {
            dir.create()
            assertTrue(dir.delete())
        }
    }

    @Test
    fun `invalid directory`() {
        val homeDir = getenv("HOME")?.toKString() ?: ""
        val dir = LocalDirectory(parentDirPath = homeDir, name = ".profile")
        runBlocking {
            assertFalse(dir.isValid())
        }
    }

    @Test
    fun `valid directory`() {
        val dir = LocalDirectory(parentDirPath = "/", name = "tmp")
        runBlocking {
            assertTrue(dir.isValid())
        }
    }

    @Test
    fun `directory permissions changed`() {
        val dir = LocalDirectory(parentDirPath = "/tmp", name = "combot_temp")
        val permissions = FilePermissions(
            owner = FilePermissionType.READ_WRITE_EXEC,
            group = FilePermissionType.READ_EXEC,
            other = FilePermissionType.NONE
        )
        runBlocking {
            dir.create()
            assertTrue(dir.changePermissions(permissions))
            dir.delete()
        }
    }
}
