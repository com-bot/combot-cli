package org.combot.cli

import kotlinx.coroutines.runBlocking
import kotlin.test.*

class LocalProgramRunnerTest {
    @Test
    fun `process ID is valid`() {
        val runner = LocalProgramRunner(programName = "kworker")
        runBlocking {
            assertTrue(runner.fetchProcessId() > -1)
        }
    }

    @Test
    fun `fetching process ID fails`() {
        val runner = LocalProgramRunner(programName = "does_not_exist")
        runBlocking {
            assertEquals(expected = -1, actual = runner.fetchProcessId())
        }
    }

    @Test
    fun `program is running`() {
        val runner = LocalProgramRunner(programName = "kworker")
        runBlocking {
            assertTrue(runner.isRunning())
        }
    }

    @Test
    fun `program is not running`() {
        val runner = LocalProgramRunner(programName = "does_not_exist")
        runBlocking {
            assertFalse(runner.isRunning())
        }
    }

    @Test
    fun `run existing program in foreground`() {
        val runner = LocalProgramRunner(programName = "printenv", programArgs = arrayOf("|", "grep", "HOME"))
        runBlocking {
            val (output, exitCode) = runner.runInForeground()
            output.toList().forEach { println("Item: $it") }
            @Suppress("ReplaceRangeToWithUntil")
            assertNotNull(output.find { it.startsWith("HOME") })
            assertEquals(expected = 0, actual = exitCode)
        }
    }

    @Test
    @Ignore
    fun `run non existing program in foreground`() {
        val runner = LocalProgramRunner(programName = "no_program")
        runBlocking {
            assertFailsWith<IllegalStateException> { runner.runInForeground() }
        }
    }
}
