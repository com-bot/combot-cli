@file:Suppress("EXPERIMENTAL_API_USAGE")

package org.combot.cli

import kotlinx.cinterop.pointed
import kotlinx.cinterop.toKString
import platform.posix.getgrgid
import platform.posix.getgrnam
import platform.posix.getpwnam
import platform.posix.getpwuid

internal actual fun fetchUserId(user: String): UInt =
    getpwnam(user)?.pointed?.pw_uid ?: throw IllegalStateException("Cannot find user")

internal actual fun fetchGroupId(group: String): UInt =
    getgrnam(group)?.pointed?.gr_gid ?: throw IllegalStateException("Cannot find group")

internal actual fun fetchUserName(uid: UInt): String =
    getpwuid(uid)?.pointed?.pw_name?.toKString() ?: ""

internal actual fun fetchGroupName(gid: UInt): String =
    getgrgid(gid)?.pointed?.gr_name?.toKString() ?: ""
