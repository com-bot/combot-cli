package org.combot.cli

import kotlinx.cinterop.*
import kotlinx.coroutines.withTimeout
import org.combot.core.ProgramRunner
import platform.posix.*

@Suppress("EXPERIMENTAL_API_USAGE", "EXPERIMENTAL_UNSIGNED_LITERALS")
public actual class LocalProgramRunner actual constructor(
    override val programName: String,
    override val programArgs: Array<String>,
    override val programDir: String,
    override val workingDir: String
) : ProgramRunner {
    override suspend fun changeEnvironmentVariable(name: String, value: String) {
        setenv(__name = name, __value = value, __replace = 1)
    }

    override suspend fun fetchProcessId(): Int = memScoped {
        val bufferSize = 8
        val buffer = allocArray<ByteVar>(bufferSize)
        val readMode = "r"
        val file = popen("pgrep $programName", readMode)
        val bytesRead = fread(__stream = file, __n = 1uL, __ptr = buffer, __size = bufferSize.toULong())
        val pid = if (bytesRead > 0uL) buffer.toKString().split("\n").first().toInt() else -1
        pclose(file)
        pid
    }

    override suspend fun isRunning(): Boolean = fetchProcessId() != -1

    private fun buildCommand() = buildString {
        if (programDir.isNotEmpty()) append("$programDir/$programName")
        else append(programName)
        programArgs.forEach { a -> append(" $a") }
    }

    override suspend fun runInBackground() {
        if (workingDir.isNotEmpty()) chdir(workingDir)
        system(buildCommand())
    }

    override suspend fun runInForeground(timeout: UInt): Pair<Array<String>, Int> = memScoped {
        val output = mutableListOf<String>()
        val readMode = "r"
        if (workingDir.isNotEmpty()) chdir(workingDir)
        val file = popen(buildCommand(), readMode)?: throw IoException("Cannot open $programName as a file")
        withTimeout(timeout.toLong()) {
            do {
                val line = file.readLine()
                if (line.isNotEmpty()) output += line.replace("\n", "")
            } while (line.isNotEmpty())
        }
        val exitCode = pclose(file)
        if (output.isNotEmpty()) output.toTypedArray() to exitCode else emptyArray<String>() to exitCode
    }

    override suspend fun terminate() {
        val pid = fetchProcessId()
        if (pid > -1) kill(pid, SIGKILL)
    }
}
