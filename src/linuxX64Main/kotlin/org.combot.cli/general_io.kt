@file:Suppress("EXPERIMENTAL_API_USAGE", "EXPERIMENTAL_UNSIGNED_LITERALS")

package org.combot.cli

import kotlinx.cinterop.*
import platform.posix.FILE
import platform.posix.getline

internal fun CPointer<FILE>.readLine(): String = memScoped {
    val buffer = alloc<CPointerVar<ByteVar>>()
    val len = alloc<ULongVar>()
    val bytesRead = getline(__lineptr = buffer.ptr, __n = len.ptr, __stream = this@readLine)
    if (bytesRead != -1L) buffer.value?.toKString() ?: "" else ""
}
