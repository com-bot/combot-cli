package org.combot.cli.fileManagement

import kotlinx.cinterop.*
import org.combot.cli.IoException
import org.combot.cli.fetchGroupId
import org.combot.cli.fetchUserId
import org.combot.core.fileManagement.File
import org.combot.core.fileManagement.PlainFile
import org.combot.core.fileManagement.authorization.FilePermissions
import platform.posix.*

@ExperimentalUnsignedTypes
public actual class LocalPlainFile actual constructor(
    override val name: String,
    override val parentDirPath: String,
    override val isBinaryFile: Boolean
) : PlainFile {
    override suspend fun isValid(): Boolean = memScoped {
        val tmp = alloc<stat>()
        stat(createFilePath(parentDirPath, name), tmp.ptr) == 0
    }

    override suspend fun size(): Long = memScoped {
        val tmp = alloc<stat>()
        stat(createFilePath(parentDirPath, name), tmp.ptr)
        if (isValid()) tmp.st_size.toLong() else -1L
    }

    override suspend fun appendBinary(vararg data: Byte): Boolean = try {
        val appendBinaryMode = "ab"
        val file = openFile(createFilePath(parentDirPath, name), appendBinaryMode)
        var bytesWritten = 0u
        data.usePinned { pinned ->
            bytesWritten = fwrite(__ptr = pinned.addressOf(0), __n = data.size.toUInt(), __s = file,
                __size = 8u)
        }
        fclose(file)
        bytesWritten > 0uL
    } catch (ex: IoException) {
        false
    }

    override suspend fun appendText(vararg data: String): Boolean = try {
        val appendMode = "a"
        val file = openFile(createFilePath(parentDirPath, name), appendMode)
        var bytesWritten = 0u
        for (item in data) {
            item.usePinned { pinned ->
                bytesWritten = fwrite(__ptr = pinned.addressOf(0), __n = 1u, __s = file,
                    __size = item.length.toUInt())
            }
            if (bytesWritten < 1uL) break
        }
        fclose(file)
        bytesWritten > 0uL
    } catch (ex: IoException) {
        false
    }

    override suspend fun changeGroup(group: String): Boolean = changeFileOwnership(
        filePath = createFilePath(parentDirPath, name),
        gid = fetchGroupId(group),
        uid = fetchUserId(fetchUser())
    )

    override suspend fun changePermissions(permissions: FilePermissions): Boolean =
        changeFilePermissions(createFilePath(parentDirPath, name), permissions)

    override suspend fun changeUser(user: String): Boolean = changeFileOwnership(
        filePath = createFilePath(parentDirPath, name),
        gid = fetchGroupId(fetchGroup()),
        uid = fetchUserId(user)
    )

    override suspend fun copy(newFile: File): Boolean {
        var result = false
        if (!isBinaryFile && newFile is PlainFile && !newFile.isBinaryFile) result = copyTextFile(newFile)
        // TODO: Handle copying a binary file.
        return result
    }

    private suspend fun moveTextFile(newFile: PlainFile): Boolean {
        var result: Boolean
        val (readSuccessful, data) = readAllText()
        if (readSuccessful) {
            result = newFile.writeText(*data)
            if (result) result = delete()
        } else {
            result = false
        }
        return result
    }

    private suspend fun copyTextFile(newFile: PlainFile): Boolean {
        val (readSuccessful, data) = readAllText()
        return if (readSuccessful) {
            newFile.writeText(*data)
        } else {
            false
        }
    }

    override suspend fun create(): Boolean = try {
        val file = openFile(createFilePath(parentDirPath, name), if (isBinaryFile) "wb" else "w")
        fclose(file)
        true
    } catch (ex: IoException) {
        false
    }

    override suspend fun move(newFile: File): Boolean {
        var result = false
        if (!isBinaryFile && newFile is PlainFile && !newFile.isBinaryFile) result = moveTextFile(newFile)
        // TODO: Handle moving a binary file.
        return result
    }

    override suspend fun readAllBinary(): Pair<Boolean, Array<Byte>> {
        TODO("Not yet implemented")
    }

    override suspend fun readAllText(): Pair<Boolean, Array<String>> = memScoped {
        val tmp = mutableListOf<String>()
        val line = alloc<CPointerVar<ByteVar>>()
        val len = alloc<UIntVar>()
        val readMode = "r"
        try {
            val file = openFile(createFilePath(parentDirPath, name), readMode)
            do {
                val bytesRead = getline(__lineptr = line.ptr, __n = len.ptr, __stream = file)
                if (bytesRead > 0) tmp += line.value?.toKString()?.replace("\n", "") ?: ""
            } while (bytesRead > 0)
            true to tmp.toTypedArray()
        } catch (ex: IoException) {
            false to emptyArray()
        }
    }

    override suspend fun readBinary(size: ULong): Pair<Boolean, Array<Byte>> {
        TODO("Not yet implemented")
    }

    override suspend fun readText(size: ULong): Pair<Boolean, String> {
        TODO("Not yet implemented")
    }

    override suspend fun delete(): Boolean = remove(createFilePath(parentDirPath, name)) == 0

    override suspend fun fetchGroup(): String = fetchFileGroup(createFilePath(parentDirPath, name))

    override suspend fun fetchGroupId(): Int = fetchFileGroupId(createFilePath(parentDirPath, name))

    override suspend fun fetchUser(): String = fetchFileUser(createFilePath(parentDirPath, name))

    override suspend fun fetchUserId(): Int = fetchFileUserId(createFilePath(parentDirPath, name))

    override suspend fun writeBinary(vararg data: Byte): Boolean = try {
        val writeMode = "wb"
        val file = openFile(createFilePath(parentDirPath, name), writeMode)
        var bytesWritten = 0u
        data.usePinned { pinned ->
            bytesWritten = fwrite(__ptr = pinned.addressOf(0), __n = data.size.toUInt(), __s = file,
                __size = 8u)
        }
        fclose(file)
        bytesWritten > 0uL
    } catch (ex: IoException) {
        false
    }

    override suspend fun writeText(vararg data: String): Boolean = try {
        val writeMode = "w"
        val file = openFile(createFilePath(parentDirPath, name), writeMode)
        var bytesWritten = 0u
        for (item in data) {
            item.usePinned { pinned ->
                bytesWritten = fwrite(__ptr = pinned.addressOf(0), __n = 1u, __s = file,
                    __size = item.length.toUInt())
            }
            if (bytesWritten < 1uL) break
        }
        fclose(file)
        bytesWritten > 0uL
    } catch (ex: IoException) {
        false
    }
}
