package org.combot.cli.fileManagement

import kotlinx.cinterop.alloc
import kotlinx.cinterop.memScoped
import kotlinx.cinterop.ptr
import org.combot.cli.fetchGroupId
import org.combot.cli.fetchUserId
import org.combot.core.fileManagement.Directory
import org.combot.core.fileManagement.File
import org.combot.core.fileManagement.authorization.FilePermissionType
import org.combot.core.fileManagement.authorization.FilePermissions
import org.combot.core.fileManagement.authorization.generateMode
import platform.posix.*

@ExperimentalUnsignedTypes
public actual class LocalDirectory actual constructor(
    override val parentDirPath: String,
    override val name: String
) : Directory {
    override suspend fun isValid(): Boolean {
        val dir = opendir(createFilePath(parentDirPath, name))
        val result = dir != null
        if (dir != null) closedir(dir)
        return result
    }

    override suspend fun size(): Long = memScoped {
        val tmp = alloc<stat>()
        stat(createFilePath(parentDirPath, name), tmp.ptr)
        if (isValid()) tmp.st_size.toLong() else -1L
    }

    override suspend fun changeGroup(group: String): Boolean = changeFileOwnership(
        filePath = createFilePath(parentDirPath, name),
        gid = fetchGroupId(group),
        uid = fetchUserId(fetchUser())
    )

    override suspend fun changePermissions(permissions: FilePermissions): Boolean =
        changeFilePermissions(createFilePath(parentDirPath, name), permissions)

    override suspend fun changeUser(user: String): Boolean = changeFileOwnership(
        filePath = createFilePath(parentDirPath, name),
        gid = fetchGroupId(fetchGroup()),
        uid = fetchUserId(user)
    )

    override suspend fun copy(newFile: File): Boolean {
        TODO("Not yet implemented")
    }

    override suspend fun create(): Boolean {
        val filePermissions = FilePermissions(
            owner = FilePermissionType.READ_WRITE_EXEC,
            group = FilePermissionType.READ_WRITE_EXEC,
            other = FilePermissionType.NONE
        )
        return mkdir(createFilePath(parentDirPath, name), filePermissions.generateMode()) == 0
    }

    override suspend fun delete(): Boolean = rmdir(createFilePath(parentDirPath, name)) == 0

    override suspend fun fetchGroup(): String = fetchFileGroup(createFilePath(parentDirPath, name))

    override suspend fun fetchGroupId(): Int = fetchFileGroupId(createFilePath(parentDirPath, name))

    override suspend fun fetchUser(): String = fetchFileUser(createFilePath(parentDirPath, name))

    override suspend fun fetchUserId(): Int = fetchFileUserId(createFilePath(parentDirPath, name))

    override suspend fun listFiles(): Pair<Boolean, Array<File>> {
        TODO("Not yet implemented")
    }

    override suspend fun move(newFile: File): Boolean {
        TODO("Not yet implemented")
    }
}
