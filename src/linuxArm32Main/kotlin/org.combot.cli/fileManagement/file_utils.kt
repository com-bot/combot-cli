@file:Suppress("EXPERIMENTAL_API_USAGE")

package org.combot.cli.fileManagement

import kotlinx.cinterop.*
import org.combot.cli.IoException
import org.combot.cli.fetchGroupName
import org.combot.cli.fetchUserName
import org.combot.core.fileManagement.authorization.FilePermissions
import org.combot.core.fileManagement.authorization.generateMode
import platform.posix.*

internal fun openFile(filePath: String, mode: String): CPointer<FILE> =
    fopen(filePath, mode) ?: throw IoException("Cannot open file")

internal actual fun createFilePath(parentDirPath: String, fileName: String): String =
    if (parentDirPath.isEmpty()) fileName else "$parentDirPath/$fileName"

internal actual fun changeFileOwnership(filePath: String, gid: UInt, uid: UInt): Boolean = chown(
    __file = filePath,
    __group = gid,
    __owner = uid
) == 0

internal actual fun fetchFileGroup(filePath: String): String = memScoped {
    val tmp = alloc<stat>()
    val rc = stat(filePath, tmp.ptr)
    if (rc == 0) fetchGroupName(tmp.st_gid) else ""
}

internal actual fun fetchFileUser(filePath: String): String = memScoped {
    val tmp = alloc<stat>()
    val rc = stat(filePath, tmp.ptr)
    if (rc == 0) fetchUserName(tmp.st_uid) else ""
}

internal actual fun changeFilePermissions(filePath: String, permissions: FilePermissions): Boolean =
    chmod(filePath, permissions.generateMode()) == 0

internal actual fun fetchFileGroupId(filePath: String): Int = memScoped {
    val tmp = alloc<stat>()
    val rc = stat(filePath, tmp.ptr)
    if (rc == 0) tmp.st_gid.toInt() else -1
}

internal actual fun fetchFileUserId(filePath: String): Int = memScoped {
    val tmp = alloc<stat>()
    val rc = stat(filePath, tmp.ptr)
    if (rc == 0) tmp.st_uid.toInt() else -1
}
